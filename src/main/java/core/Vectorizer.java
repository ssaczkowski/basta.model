package core;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

public class Vectorizer {

    public void initVectorizer() {
        chargeDictionary();
    }

    public static int[] getVector(String msg) {

        ArrayList<String> ngrams = getTokens(msg);

        return createVector(ngrams);
    }

    private static int[] createVector(ArrayList<String> ngrams) {

        int cont = 0;
        int[] vectores = new int[DictionaryRegistry.getInstance().size()];
        for (int i = 0; i < vectores.length; i++) {
            vectores[i] = 0;
        }

        for (String s : ngrams) {
            try {
                if (DictionaryRegistry.getInstance().containsKey(s)) {
                    int index = DictionaryRegistry.getInstance().get(s);
                    vectores[index] = 1;
                    cont = cont + 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (cont > 0)
            return vectores;
        else
            return null;
    }

    private static ArrayList<String> getTokens(String msg) {

        ArrayList<String> tokens = new ArrayList<>();
        PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<>(new StringReader(msg),
                new CoreLabelTokenFactory(), "");
        for (CoreLabel label; ptbt.hasNext(); ) {
            label = ptbt.next();
            tokens.add(label.value());
        }

        ArrayList<String> ngrams = new ArrayList<>(tokens);

        for (int i = 0; i < tokens.size() - 1; i++) {
            ngrams.add(tokens.get(i) + " " + tokens.get(i + 1));
        }

        for (int i = 0; i < tokens.size() - 2; i++) {
            ngrams.add(tokens.get(i) + " " + tokens.get(i + 1) + " " + tokens.get(i + 2));
        }

        return ngrams;
    }

    private void chargeDictionary() {

        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("dictionary.json")) {
            //Read JSON file
            JSONObject features = (JSONObject) jsonParser.parse(reader);

            for (String key : (Iterable<String>) features.keySet()) {
                Long value = (Long) features.get(key);
                DictionaryRegistry.getInstance().put(key, Integer.valueOf(String.valueOf(value)));
            }

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
