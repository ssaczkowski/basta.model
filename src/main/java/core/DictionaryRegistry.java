package core;

import java.util.HashMap;
import java.util.Map;

public class DictionaryRegistry {

    private static Map<String, Integer> INSTANCE;

    public static Map<String, Integer> getInstance() {

        if (INSTANCE != null) {
            return INSTANCE;
        } else {
            INSTANCE = new HashMap<>();
            return INSTANCE;
        }
    }
}
