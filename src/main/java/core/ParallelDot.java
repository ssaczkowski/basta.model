package core;

import com.paralleldots.paralleldots.App;
import twitter4j.JSONObject;

public class ParallelDot {

    private static ParallelDot instance = null;
    private static App pd;

    private ParallelDot() {

    }

    public static ParallelDot getInstance() {

        if (instance == null) {
            pd = new App("9wecQuCQpQ12qQvnSrcB0xlxTj6cdZ8ltClcy6FWarw");
            instance = new ParallelDot();
        }
        return instance;
    }

    public String emotion(String msg) {
        String emotion = null;
        try {
            return pd.emotion(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return emotion;
    }

    public String abuse(String msg) {
        String abuse = null;
        try {
            return pd.abuse(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return abuse;
    }
}