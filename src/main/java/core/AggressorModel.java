package core;

import org.deeplearning4j.nn.modelimport.keras.KerasModelImport;
import org.deeplearning4j.nn.modelimport.keras.exceptions.InvalidKerasConfigurationException;
import org.deeplearning4j.nn.modelimport.keras.exceptions.UnsupportedKerasConfigurationException;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AggressorModel {


    private static AggressorModel instance = null;

    private AggressorModel() {

    }

    public static AggressorModel getInstance() {

        if (instance == null) {
            instance = new AggressorModel();
        }
        return instance;
    }

    public String prediction(String msg) {
        String result = "";
        Process p;
        try{

            String[] cmd = {
                    "/bin/bash",
                    "-c",
                    "python make_prediction.py '" + msg + "'"
            };

            p = Runtime.getRuntime().exec(cmd);

            p.waitFor();


            BufferedReader in = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));

            StringBuilder buffer = new StringBuilder();

            String line = null;

            while ((line = in.readLine()) != null){
                buffer.append(line);
            }

            int exitCode = p.waitFor();

            /*System.out.println("Value is: "+buffer.toString());
            System.out.println("Process exit value:"+exitCode);*/

            result = buffer.toString();

            in.close();

            p.destroy();

        } catch (Exception e) {}


        return result;
    }
}
