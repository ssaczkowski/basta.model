package basta.exporter;

import basta.structure.Message;
import com.google.gson.Gson;

import java.io.*;

public class StrategyExportTxt implements IStrategyExport {


    @Override
    public void export(Message message) {

        Gson gson = new Gson();
        String messageJson = gson.toJson(message);
        String seqName = message.getHeaderMessage().getAttributes().get("sequenceName");

        if (seqName != null) {

            BufferedWriter writer = null;
            try {

                writer = new BufferedWriter(new FileWriter(seqName + "messages.txt", true));
                writer.append(messageJson);
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String lastLine(String seqName) throws IOException {
        String last = null;
        if (seqName != null) {
            BufferedReader input = new BufferedReader(new FileReader(seqName + "messages.txt"));

            String line;

            while ((line = input.readLine()) != null) {
                last = line;
            }
            input.close();

        }
        return last;
    }
}