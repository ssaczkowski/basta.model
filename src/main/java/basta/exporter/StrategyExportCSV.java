package basta.exporter;

import basta.structure.Message;
import com.google.gson.Gson;
import twitter4j.JSONObject;

import java.io.*;

public class StrategyExportCSV implements IStrategyExport {

    @Override
    public void export(Message message) {

        Gson gson = new Gson();
        String messageJson = gson.toJson(message);
        JSONObject json = new JSONObject(messageJson);
        String seqName = message.getHeaderMessage().getAttributes().get("sequenceName");

        if (seqName != null) {

            BufferedWriter writer;
            try {

                writer = new BufferedWriter(new FileWriter(seqName + ".csv", true));
                writer.append(json.getJSONObject("body").getString("value"));
                writer.append(",");
                //writer.append(json.getJSONObject("headerMessage").getJSONObject("attributes").getString("userScreenName"));
                writer.append("\n");
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
