package basta.exporter;

import basta.structure.Message;

public interface IStrategyExport {

    public void export(Message message);
    
}

