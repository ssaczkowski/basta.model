package basta.exporter;

import co.aurasphere.jyandex.Jyandex;
import co.aurasphere.jyandex.dto.Language;
import core.ParallelDot;
import twitter4j.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;

public class ImporterCSV {

    private String path;

    public ImporterCSV(String path) {
        this.path = path;
    }

    public void getLines() {
        try {
            String row;
            String text;
            Double abuse = 0.0;
            Double noAbuse = 0.0;
            BufferedReader csvReader = new BufferedReader(new FileReader(this.path));
            BufferedWriter csvWriter = new BufferedWriter(new FileWriter("etiqueted.csv", true));
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(",");
                Jyandex client = new Jyandex("trnsl.1.1.20190920T180203Z.e0451d41fe7377b3.169d6cfdb8345ec7c342ceed1cc3a144222ea638");
                text = Arrays.toString(client.translateText(data[0], Language.SPANISH, Language.ENGLISH).getTranslatedText());

                JSONObject jsonObject = new JSONObject(ParallelDot.getInstance().abuse(text));

                csvWriter.append(data[0]).append(",");
                abuse = Double.valueOf(jsonObject.getString("abusive"));
                noAbuse = Double.valueOf(jsonObject.getString("neither"));
                if (abuse < noAbuse) {
                    csvWriter.append("0");
                } else {
                    csvWriter.append("1");
                }
                csvWriter.append("\n");
            }
            csvReader.close();
            csvWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
