package basta.exporter;

import basta.structure.Message;
import com.google.gson.Gson;
import twitter4j.JSONObject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class StrategyExportEvaluate implements IStrategyExport {

    @Override
    public void export(Message message) {
        Gson gson = new Gson();
        String messageJson = gson.toJson(message);
        JSONObject json = new JSONObject(messageJson);

        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter("evaluation.csv", true));
            writer.append(json.getJSONObject("body").getString("value"));
            writer.append(",");
            writer.append(json.getJSONObject("headerMessage").getJSONObject("attributes").getString("label"));
            writer.append(",");
            writer.append(json.getJSONObject("headerMessage").getJSONObject("attributes").getString("result"));
            writer.append("\n");
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
