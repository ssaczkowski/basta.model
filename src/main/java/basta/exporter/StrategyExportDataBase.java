package basta.exporter;

import basta.connection.database.dao.MongoDAO;
import basta.structure.Message;

public class StrategyExportDataBase implements IStrategyExport {

    private MongoDAO mongo;

    public StrategyExportDataBase() {
        this.mongo = new MongoDAO();
    }

    @Override
    public void export(Message message) {

        if (message != null) {
            try {
                mongo.insertMessage(message.getBody());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
