package basta.model;

public class Assignment {

    private String user;

    private String tweet;

    private String label;


    public Assignment(String user, String tweet, String label) {
        this.user = user;
        this.tweet = tweet;
        this.label = label;
    }


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
