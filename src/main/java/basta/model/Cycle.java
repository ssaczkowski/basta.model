package basta.model;

import java.util.ArrayList;

public class Cycle {

    private String user;

    private ArrayList<String> msgs;

    private String fase;

    public Cycle(String user, ArrayList<String> msgs, String fase) {
        this.user = user;
        this.msgs = msgs;
        this.fase = fase;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public ArrayList<String> getMsgs() {
        return msgs;
    }

    public void setMsgs(ArrayList<String> msgs) {
        this.msgs = msgs;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }
}
