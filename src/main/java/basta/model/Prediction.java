package basta.model;

public class Prediction {

    private String message;

    private String value;

    private String user;


    public Prediction(String message, String value, String user) {
        this.message = message;
        this.value = value;
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
