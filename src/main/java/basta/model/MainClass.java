package basta.model;

import basta.connection.database.dao.MongoDAO;
import basta.structure.Sequencer;
import core.Vectorizer;

import java.util.ArrayList;

public class MainClass {

    public static void main(String[] args) {

        BastaModel model = BastaModel.getInstance();

        //model.fit_model(); // NO DESCOMENTAR YA QUE SE CUELGA ACA! HAY QUE VER POR Q , SI SOLO HAY QUE ESPERAR O NUNCA TERMINA EL PROCESO.

        Sequencer seq = model.aggresorSequence();
        model.analize(seq);
        model.start();
    }
}