package basta.model;

import basta.connection.database.dao.MongoDAO;
import basta.connection.networks.TweetsByQuery;
import basta.connection.networks.TweetsByStream;
import basta.exporter.StrategyExportCSV;
import basta.exporter.StrategyExportDataBase;
import basta.filters.*;
import basta.structure.Filter;
import basta.structure.Sequencer;
import basta.structure.channel.pubsubchannel.Event;

import java.io.*;
import java.util.ArrayList;


public class BastaModel extends Thread {

    private static BastaModel instance = null;
    private static MongoDAO mongo = new MongoDAO();

    public static BastaModel getInstance() {

        if (instance == null) {
            instance = new BastaModel();
        }
        return instance;
    }

    @Override
    public void run() {
        super.run();
        try {
            if (mongo.dataBaseLow()) {
                Sequencer seq = botSequence();
                fillBase(seq);
            }
            sleep(3600000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Sequencer aggresorSequence() {
        ArrayList<Filter> filters = new ArrayList<>();

        RepeatedSpamFilter rsf = new RepeatedSpamFilter("RepeatedSpam");
        HashtagSpamFilter sf = new HashtagSpamFilter("Spam");
        PreProcessFilter ppf = new PreProcessFilter("PreProcess");
        RepetitiveCharFilter rcf = new RepetitiveCharFilter("Repetitive");
        StepWordsFilter sw = new StepWordsFilter("StepWords");
        AggressorFilter af = new AggressorFilter("Aggressor");
        EmotionFilter ef = new EmotionFilter("Emotion");
        ExporterFilter ex = new ExporterFilter("Exporter", new StrategyExportCSV());

        filters.add(rsf);
        filters.add(sf);
        filters.add(ppf);
        filters.add(rcf);
        filters.add(sw);
        filters.add(af);
        filters.add(ef);
        filters.add(ex);

        return new Sequencer(filters);
    }

    public Sequencer botSequence() {
        ArrayList<Filter> filters = new ArrayList<>();

        RepeatedSpamFilter rsf = new RepeatedSpamFilter("RepeatedSpam");
        HashtagSpamFilter sf = new HashtagSpamFilter("Spam");
        RepetitiveCharFilter rcf = new RepetitiveCharFilter("Repetitive");
        AggressorFilter ag = new AggressorFilter("Aggressor");
        ExporterFilter ex = new ExporterFilter("Exporter", new StrategyExportDataBase());

        filters.add(rsf);
        filters.add(sf);
        filters.add(rcf);
        //filters.add(ag);
        //filters.add(ex);

        return new Sequencer(filters);
    }

    public Sequencer evaluateSequence() {
        ArrayList<Filter> filters = new ArrayList<>();

        RepeatedSpamFilter rsf = new RepeatedSpamFilter("RepeatedSpam");
        HashtagSpamFilter sf = new HashtagSpamFilter("Spam");
        PreProcessFilter ppf = new PreProcessFilter("PreProcess");
        RepetitiveCharFilter rcf = new RepetitiveCharFilter("Repetitive");
        StepWordsFilter sw = new StepWordsFilter("StepWords");
        EvaluateFilter ef = new EvaluateFilter("Evaluate");

        filters.add(rsf);
        filters.add(sf);
        filters.add(ppf);
        filters.add(rcf);
        filters.add(sw);
        filters.add(ef);

        return new Sequencer(filters);
    }

    public void analize(Sequencer sequencer) {

        Event.operation.subscribe("StreamingTweets", sequencer.getPipeFilter());
        TweetsByStream tweets = new TweetsByStream();
        tweets.startStream();

        Thread seaThread = new Thread() {
            public void run() {
                super.run();
                try {
                    while (true) {
                        sequencer.getPipeFilter().filter();
                        sleep(30000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        seaThread.start();
    }

    public void fillBase(Sequencer sequencer) {
        Event.operation.subscribe("BotBase", sequencer.getPipeFilter());
        TweetsByQuery tweets = new TweetsByQuery();
        tweets.searchAll();
        sequencer.getPipeFilter().filter();
    }

    public void fit_model() {

        Process p;
        try {

            String[] cmd = {
                    "/bin/bash",
                    "-c",
                    "python fit_model.py"
            };

            p = Runtime.getRuntime().exec(cmd);

            p.waitFor();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));

            StringBuilder buffer = new StringBuilder();

            String line = null;

            while ((line = in.readLine()) != null) {
                buffer.append(line);
            }

            int exitCode = p.waitFor();

            System.out.println("Value is: " + buffer.toString());
            System.out.println("Process exit value:" + exitCode);

            in.close();

            p.destroy();

        } catch (Exception e) {
        }

    }

    public ArrayList<Prediction> getPredictions() {
        return mongo.getPredictions();
    }

    /*public ArrayList<Cycle> getCycles() {
        return mongo.getCycles();
    }*/

/*
    public ArrayList<Prediction> getPredictionsByCSV() {

        ArrayList<Prediction> predictionsResult = new ArrayList<>();

        String csvFile = "predictions";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] data = line.split(cvsSplitBy);

                Prediction p = new Prediction(data[0], data[1], "");

                predictionsResult.add(p);

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return predictionsResult;
    }
*/
}