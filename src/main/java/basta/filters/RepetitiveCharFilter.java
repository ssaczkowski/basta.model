package basta.filters;

import basta.structure.Filter;
import basta.structure.Message;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class RepetitiveCharFilter extends Filter {

    public RepetitiveCharFilter(String name) {
        super(name);
    }

    @Override
    public void filter() {
        while (!this.getIQueueIn().isEmpty()) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {

                this.removeRepetitive(message);
                this.getIQueueOut().put(message);
                //this.filter();
            }
        }
    }


    private void removeRepetitive(Message m) {

        String text = m.getBody();
        ArrayList<String> tokens = new ArrayList<>();

        PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<>(new StringReader(text),
                new CoreLabelTokenFactory(), "");
        for (CoreLabel label; ptbt.hasNext(); ) {
            label = ptbt.next();
            tokens.add(label.value());  //Genero los tokens para revisar palabra a palabra.
        }

        text = "";
        String regex = "(?i)([a-z])\\1{2,}";    //Es sensitivo a las mayusculas. Si existen mas de 2 caracteres iguales
        //seguidos hace match.
        for (String string : tokens) {
            string = string.replaceAll(regex, "$1"); //El $1 significa backreference.
            text = text.concat(" " + string); //Concateno los tokens para formar el mensaje original.
        }

        m.setBody(text.trim()); //Trim() borra el espacio en blanco inicial y final (si los hay) y ademas
        //reemplaza los espacios en blanco consecutivos por un unico espacio en blanco.
    }
}
