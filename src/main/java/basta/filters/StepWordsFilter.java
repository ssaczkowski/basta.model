package basta.filters;

import basta.structure.Filter;
import basta.structure.Message;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;

import java.io.StringReader;
import java.util.ArrayList;

public class StepWordsFilter extends Filter {

    public StepWordsFilter(String name) {
        super(name);
    }

    @Override
    public void filter() {

        while (!this.getIQueueIn().isEmpty()) {
            Message message = this.getIQueueIn().getMessage();

            if (message != null) {

                this.removeStepWords(message);
                this.getIQueueOut().put(message);
                //this.filter();
            }
        }
    }


    private void removeStepWords(Message m) {

        String text = m.getBody().toLowerCase();
        ArrayList<String> tokens = new ArrayList<>();

        ArrayList<String> stepWords = new ArrayList<>();
        stepWords.add("la");
        stepWords.add("el");
        stepWords.add("los");
        stepWords.add("las");
        stepWords.add("un");
        stepWords.add("una");
        stepWords.add("unos");
        stepWords.add("unas");
        stepWords.add("al");
        stepWords.add("del");
        stepWords.add("lo");

        PTBTokenizer<CoreLabel> ptbt = new PTBTokenizer<>(new StringReader(text),
                new CoreLabelTokenFactory(), "");
        for (CoreLabel label; ptbt.hasNext(); ) {
            label = ptbt.next();
            tokens.add(label.value());  //Genero los tokens para revisar palabra a palabra.
        }

        text = "";

        for (String string : tokens) {
            if (!stepWords.contains(string)) {
                text = text.concat(" " + string); //Concateno los tokens para formar el mensaje original.

            }
        }

        m.setBody(text.trim()); //Trim() borra el espacio en blanco inicial y final (si los hay) y ademas
        //reemplaza los espacios en blanco consecutivos por un unico espacio en blanco.
    }
}
