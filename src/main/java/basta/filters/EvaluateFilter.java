package basta.filters;

import basta.exporter.Exporter;
import basta.exporter.StrategyExportEvaluate;
import basta.structure.Filter;
import basta.structure.Message;
import core.AggressorModel;

public class EvaluateFilter extends Filter {

    private Exporter exporter;

    public EvaluateFilter(String name) {
        super(name);
        exporter = new Exporter(new StrategyExportEvaluate());
    }

    @Override
    public void filter() {
        while (!this.getIQueueIn().isEmpty()) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {

                if (esViolento(message)) {
                    message.addAttribute("result", "violento");
                    exporter.export(message);
                } else {
                    message.addAttribute("result", "noViolento");
                    exporter.export(message);
                }
            }
        }
    }

    private boolean esViolento(Message m) {
        AggressorModel agreModel = AggressorModel.getInstance();

        String prediction = agreModel.prediction(m.getBody());

        if(prediction.equals("1")){
            return true;
        }
        return false;
    }
}