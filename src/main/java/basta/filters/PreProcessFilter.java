package basta.filters;

import basta.structure.Filter;
import basta.structure.Message;

public class PreProcessFilter extends Filter {

    public PreProcessFilter(String name) {
        super(name);
    }

    @Override
    public void filter() {
        while (!this.getIQueueIn().isEmpty()) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {

                this.clean(message);
                this.getIQueueOut().put(message);
                //this.filter();
            }
        }
    }


    private void clean(Message m) {

        String text = m.getBody();

        text = text.replaceAll("á", "a");
        text = text.replaceAll("é", "e");
        text = text.replaceAll("í", "i");
        text = text.replaceAll("ó", "o");
        text = text.replaceAll("ú", "u");
        text = text.replaceAll("ñ", "n");
        text = text.replaceAll("ü", "u");


        String urlIdentifier = "((http|ftp|https):\\/\\/)?[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?";

        //Removes URL's, RT, Twitter usernames and any non alpha numeric character
        String[] removeNoise = {"RT", urlIdentifier, "(?:\\s|\\A)[@]+([A-Za-z0-9-_]+)", "[^a-zA-Z ]"};

        for (String s : removeNoise) {
            text = text.replaceAll(s, "");
        }

        text = text.toLowerCase();

        m.setBody(text.trim());

    }
}