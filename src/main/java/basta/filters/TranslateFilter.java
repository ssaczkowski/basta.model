package basta.filters;

import basta.structure.Filter;
import basta.structure.Message;
import co.aurasphere.jyandex.Jyandex;
import co.aurasphere.jyandex.dto.Language;

import java.util.Arrays;

public class TranslateFilter extends Filter {

    public TranslateFilter(String name) {
        super(name);
    }

    @Override
    public void filter() {

        while (!this.getIQueueIn().isEmpty()) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {

                String text = "";
                //Jyandex client = new Jyandex("trnsl.1.1.20190920T180203Z.e0451d41fe7377b3.169d6cfdb8345ec7c342ceed1cc3a144222ea638");

                //text = Arrays.toString(client.translateText(message.getBody(), Language.SPANISH, Language.ENGLISH).getTranslatedText());

                //message.setBody(text);

                this.getIQueueOut().put(message);
                //this.filter();
            }
        }
    }
}