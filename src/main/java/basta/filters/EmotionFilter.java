package basta.filters;

import basta.connection.database.dao.MongoDAO;
import basta.connection.networks.TweetsByUser;
import basta.exporter.StrategyExportCSV;
import basta.structure.Emotion;
import basta.structure.Filter;
import basta.structure.Message;
import core.ParallelDot;
import twitter4j.JSONObject;
import twitter4j.TwitterException;

import java.util.ArrayList;
import java.util.List;

public class EmotionFilter extends Filter {

    private MongoDAO mongo;

    public EmotionFilter(String name) {
        super(name);
        mongo = new MongoDAO();
    }

    @Override
    public void filter() {
        while (!this.getIQueueIn().isEmpty()) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {
                try {
                    StrategyExportCSV stra = new StrategyExportCSV();
                    TweetsByUser utl = new TweetsByUser(Long.valueOf(message.getHeaderMessage().getAttributes().get("idMessage")), message.getHeaderMessage().getAttributes().get("userScreenName"));
                    List<Message> msgs = utl.getDataFrom();

                    switch (detectarFases(msgs, message.getHeaderMessage().getAttributes().get("userScreenName"))) {
                        case 0: //No cumple con el ciclo de la violencia.
                            message.getHeaderMessage().getAttributes().put("sequenceName", "NoCumple");
                            break;
                        case 1: //Cumple con el ciclo de la violencia "luna de miel", "tension", y "agresion".
                            message.getHeaderMessage().getAttributes().put("sequenceName", "Cumple");
                            break;
                    }
                    stra.export(message);
                    break;

                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private int detectarFases(List<Message> msgs, String user) {

        int res = 0;
        ArrayList<Emotion> probas = getProbabilities(msgs);

        int largo = probas.size();
        int window = 5;

        while (window < largo) {
            ArrayList<Emotion> session = getProms(probas, window);
            if (session != null) {
                res = getFasesCount(session, user);
                if (res == 1) {
                    return res;
                }
            }
            window = window + 1;
        }
        return res;
    }

    private int getFasesCount(ArrayList<Emotion> proms, String user) {
        for (int i = 0; i < proms.size(); i++) {
            if (proms.get(i).getMax().equals("tension")) {
                if (i + 1 < proms.size()) {
                    if (proms.get(i + 1).getMax().equals("luna")) {
                        mongo.insertCicle(user, proms.get(i).getFrom(), "Tensión");
                        mongo.insertCicle(user, proms.get(i).getFrom(), "Luna de Miél");
                        return 1;
                    }
                }
            }
        }
        return 0;
    }

    private ArrayList<Emotion> getProms(ArrayList<Emotion> probabilities, int window) {

        ArrayList<Emotion> proms = new ArrayList<>();

        Double angry = 0.0;
        Double happy = 0.0;
        Double excited = 0.0;
        Double fear = 0.0;
        Double sad = 0.0;
        Double bored = 0.0;

        Emotion e = new Emotion();

        for (int i = 0; i <= probabilities.size() - window; i++) {
            for (int j = 0; j < window; j++) {
                e.getFrom().add(probabilities.get(i + j).getFrom().get(0));
                angry = angry + probabilities.get(i + j).getEmotions().get("angry");
                happy = happy + probabilities.get(i + j).getEmotions().get("happy");
                excited = excited + probabilities.get(i + j).getEmotions().get("excited");
                fear = fear + probabilities.get(i + j).getEmotions().get("fear");
                sad = sad + probabilities.get(i + j).getEmotions().get("sad");
                bored = bored + probabilities.get(i + j).getEmotions().get("bored");
            }

            e.addEmotion("angry", angry);
            e.addEmotion("happy", happy);
            e.addEmotion("excited", excited);
            e.addEmotion("fear", fear);
            e.addEmotion("sad", sad);
            e.addEmotion("bored", bored);
            proms.add(e);
            angry = 0.0;
            happy = 0.0;
            excited = 0.0;
            fear = 0.0;
            sad = 0.0;
            bored = 0.0;
        }
        return proms;
    }

    private ArrayList<Emotion> getProbabilities(List<Message> msgs) {
        ArrayList<Emotion> probabilities = new ArrayList<>();

        System.out.println("Entre al probabilities.");

        int cont = 1;

        for (Message m :
                msgs) {
            try {
                System.out.println("Mensaje n°:" + cont + m.getBody());
                Emotion e = new Emotion();

                JSONObject jsonObject = new JSONObject(ParallelDot.getInstance().emotion(m.getBody()));
                Double angry = Double.valueOf(jsonObject.getJSONObject("emotion").getJSONObject("probabilities").getString("Angry"));
                Double happy = Double.valueOf(jsonObject.getJSONObject("emotion").getJSONObject("probabilities").getString("Happy"));
                Double excited = Double.valueOf(jsonObject.getJSONObject("emotion").getJSONObject("probabilities").getString("Excited"));
                Double fear = Double.valueOf(jsonObject.getJSONObject("emotion").getJSONObject("probabilities").getString("Fear"));
                Double sad = Double.valueOf(jsonObject.getJSONObject("emotion").getJSONObject("probabilities").getString("Sad"));
                Double bored = Double.valueOf(jsonObject.getJSONObject("emotion").getJSONObject("probabilities").getString("Bored"));

                e.getFrom().add(m.getBody());
                e.addEmotion("angry", angry);
                e.addEmotion("happy", happy);
                e.addEmotion("excited", excited);
                e.addEmotion("fear", fear);
                e.addEmotion("sad", sad);
                e.addEmotion("bored", bored);

                probabilities.add(e);
                cont += 1;
            } catch (Exception e) {
                cont += 1;
            }
        }
        return probabilities;
    }
}