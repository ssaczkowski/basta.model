package basta.filters;

import basta.exporter.Exporter;
import basta.exporter.IStrategyExport;
import basta.structure.Filter;
import basta.structure.Message;

public class ExporterFilter extends Filter {

    private Exporter exporter;

    public ExporterFilter(String name, IStrategyExport strategy) {
        super(name);
        exporter = new Exporter(strategy);
    }

    @Override
    public void filter() {
        while (!this.getIQueueIn().isEmpty()) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {

                exporter.export(message);
                break;

            }
        }
    }
}
