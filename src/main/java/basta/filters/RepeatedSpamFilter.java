package basta.filters;

import basta.structure.Filter;
import basta.structure.Message;
import basta.structure.messageRegistry;

import java.util.ArrayList;
import java.util.List;

public class RepeatedSpamFilter extends Filter {

    public RepeatedSpamFilter(String name) {
        super(name);
    }

    @Override
    public void filter() {
        while (!this.getIQueueIn().isEmpty()) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {

                if (this.noRepeated(message)) {
                    this.getIQueueOut().put(message);
                }
                //this.filter();
            }
        }
    }


    private boolean noRepeated(Message m) {

        String user = dameUser(m);

        if (messageRegistry.getInstance().get(user) == null) {
            List<String> msgList = new ArrayList<>();//TODO: poner en el get el @usuario.
            msgList.add(m.getBody());
            messageRegistry.getInstance().put(user, msgList);
            return true;
        } else {
            if (!messageRegistry.getInstance().get(user).contains(m.getBody())) {
                messageRegistry.getInstance().get(user).add(m.getBody());
                return true;
            } else {
                return false;
            }
        }
    }

    private String dameUser(Message m) {
        char[] chars = m.getBody().toCharArray();
        String aux = "";
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '@') {
                aux = aux.concat("@");
                for (int j = i + 1; j < chars.length; j++) {
                    if (chars[j] != ' ') {
                        aux = aux.concat(String.valueOf(chars[j]));
                    } else {
                        break;
                    }
                }
                break;
            }
        }
        return aux;
    }
}