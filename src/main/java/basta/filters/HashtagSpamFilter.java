package basta.filters;

import basta.structure.Filter;
import basta.structure.Message;

public class HashtagSpamFilter extends Filter {

    public HashtagSpamFilter(String name) {
        super(name);
    }

    @Override
    public void filter() {
        while (!this.getIQueueIn().isEmpty()) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {

                String aux = message.getBody();
                aux = aux.replaceAll("[^#]", "");

                if (aux.length() <= 5) {
                    this.getIQueueOut().put(message);
                    //this.filter();
                }
            }
        }
    }

}
