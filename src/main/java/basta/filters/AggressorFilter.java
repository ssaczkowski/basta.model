package basta.filters;

import basta.connection.database.dao.MongoDAO;
import basta.model.Prediction;
import basta.structure.Filter;
import basta.structure.Message;
import core.AggressorModel;

public class AggressorFilter extends Filter {

    private MongoDAO mongo;

    public AggressorFilter(String name) {
        super(name);
        this.mongo = new MongoDAO();
    }

    @Override
    public void filter() {
        while (!this.getIQueueIn().isEmpty()) {

            Message message = this.getIQueueIn().getMessage();

            if (message != null) {
                if (esViolento(message.getBody())) {
                    Prediction p = new Prediction(message.getBody(), "1", message.getHeaderMessage().getAttributes().get("user"));
                    mongo.insertPrediction(p);
                    this.getIQueueOut().put(message);
                } else {
                    Prediction p = new Prediction(message.getBody(), "0", message.getHeaderMessage().getAttributes().get("user"));
                    mongo.insertPrediction(p);
                }
            }
        }
    }

    private boolean esViolento(String message) {

        String pred = AggressorModel.getInstance().prediction(message);

        return pred.equals("1");
    }
}