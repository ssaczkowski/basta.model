package basta.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Emotion {

    private HashMap<String, Double> emotions;
    private ArrayList<String> from;

    public Emotion() {
        this.emotions = new HashMap<>();
        this.from = new ArrayList<>();
    }

    public HashMap<String, Double> getEmotions() {
        return emotions;
    }

    public ArrayList<String> getFrom() {
        return this.from;
    }

    public void addEmotion(String key, Double value) {
        this.emotions.put(key, value);
    }

    public String getMax() {
        Iterator hmIterator = emotions.entrySet().iterator();

        Double luna = 0.0;
        Double tension = 0.0;
        Double otros = 0.0;

        while (hmIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry) hmIterator.next();
            if (mapElement.getKey().equals("happy") || mapElement.getKey().equals("excited")) {
                luna = luna + (Double) mapElement.getValue();
            } else if (mapElement.getKey().equals("sad") || mapElement.getKey().equals("bored")
                    || mapElement.getKey().equals("fear")) {
                otros = otros + (Double) mapElement.getValue();
            } else {
                tension = tension + (Double) mapElement.getValue();
            }
        }

        if (luna > tension && luna > otros) {
            return "luna";
        } else if (otros > luna && otros > tension) {
            return "otros";
        } else {
            return "tension";
        }
    }
}