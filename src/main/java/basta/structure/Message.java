package basta.structure;

public class Message {

    private BodyMessage body;
    private HeaderMessage headerMessage;


    public Message() {
        this.body = new BodyMessage();
        this.headerMessage = new HeaderMessage();

    }

    public String getBody() {
        return this.body.getValue();
    }

    public void setBody(String value) {
        body.setValue(value);
    }

    public void addAttribute(String key, String value) {
        this.headerMessage.addAttribute(key, value);
    }

    public HeaderMessage getHeaderMessage() {
        return this.headerMessage;
    }

    @Override
    public String toString() {
        return "Message{" + "Body=" + body + ", Header=" + headerMessage + '}';
    }

}
