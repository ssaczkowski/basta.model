package basta.structure.channel;

import basta.structure.Message;

public interface IQueue {

    public void put(Message message);
    public Message getMessage();
    public int size();
    public boolean isEmpty();
}
