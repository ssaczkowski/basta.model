package basta.structure.channel;

import basta.structure.Message;

import java.util.ArrayList;

public class Queue implements IQueue {

    private ArrayList<Message> queue;

    public Queue(){
        queue = new ArrayList<>();
    }

    @Override
    public void put(Message Message) {
        queue.add(Message);
    }

    @Override
    public Message getMessage() {
        Message message = null;

        if(!queue.isEmpty()){
            int position =  this.queue.size()-1;

            message = this.queue.get(position);

            this.queue.remove(position);
        }

        return message;
    }

    @Override
    public int size() {
        return queue.size();
    }

    @Override
    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
