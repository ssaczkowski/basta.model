package basta.structure;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class messageRegistry {

    private static Map<String, List<String>> INSTANCE;

    public static Map<String, List<String>> getInstance() {
        if (INSTANCE != null) {
            return INSTANCE;
        } else {
            INSTANCE = new HashMap<>();
            return INSTANCE;
        }
    }
}