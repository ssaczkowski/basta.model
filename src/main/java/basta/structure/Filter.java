package basta.structure;

import basta.structure.channel.IQueue;
import basta.structure.channel.Queue;

public class Filter implements IFilter, INameable {

    private IQueue queueIn;
    private IQueue queueOut;
    private String name;

    public Filter(String name) {
        this.name = name;
        this.queueIn = new Queue();
        this.queueOut = new Queue();
    }

    @Override
    public void filter() {

    }

    @Override
    public IQueue getIQueueIn() {
        return this.queueIn;
    }

    @Override
    public void setIQueueIn(IQueue iQueue) {
        this.queueIn = iQueue;
    }

    @Override
    public IQueue getIQueueOut() {
        return this.queueOut;
    }

    @Override
    public void setIQueueOut(IQueue iQueue) {
        this.queueOut = iQueue;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Filter{ name='" + name + '\'' + "}'";
    }
}
