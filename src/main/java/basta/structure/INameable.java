package basta.structure;

public interface INameable {

    public String getName();
}
