package basta.structure;

import java.util.List;

public class Sequencer {

    private PipeFilter pipeFilter = new PipeFilter();

    public Sequencer(List<Filter> filters) {

        if (filters != null) {
            for (Filter f : filters) {
                pipeFilter.add(f);
            }
        }
    }

    public PipeFilter getPipeFilter() {
        return this.pipeFilter;
    }
}

