package basta.connection.networks;

import basta.connection.netconfiguration.Configuration;
import basta.structure.Message;
import basta.structure.channel.pubsubchannel.Event;
import basta.util.ConfigurationMnemonic;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

public class TweetsByQuery {

    private Twitter connection;
    private Query query;

    public TweetsByQuery() {
        setQuery();
        this.connectTwitter();
    }

    private void setQuery() {

        query = new Query();
        query.count(100);
        query.resultType(Query.ResultType.mixed);
        query.setLang("es");
        query.setQuery("@");
    }

    private void connectTwitter() {

        Configuration configuration = new Configuration();

        configuration.putParameter(ConfigurationMnemonic.CONSUMER_KEY, "XHQ6DGnOInfr3xmlv6Tjkf86y");
        configuration.putParameter(ConfigurationMnemonic.CONSUMER_SECRET, "E8thwH5N8ig8IaXhazTMWIbOOMSEJVGTZOF3loNVtr1evaSoTl");
        configuration.putParameter(ConfigurationMnemonic.TOKEN_KEY, "1119972818404376576-oCV1fJ9QRmuR7ux3JxFfCITZtCMzUI");
        configuration.putParameter(ConfigurationMnemonic.TOKEN_SECRET, "ZYsLreqLDUBmvzunR1qrHpM84KjuPDVBlwMFLsUwS2fkE");

        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.setOAuthConsumerKey(configuration.getParameter(ConfigurationMnemonic.CONSUMER_KEY));
        builder.setOAuthConsumerSecret(configuration.getParameter(ConfigurationMnemonic.CONSUMER_SECRET));
        builder.setOAuthAccessToken(configuration.getParameter(ConfigurationMnemonic.TOKEN_KEY));
        builder.setOAuthAccessTokenSecret(configuration.getParameter(ConfigurationMnemonic.TOKEN_SECRET));
        builder.setTweetModeExtended(true);
        twitter4j.conf.Configuration configurationBuilder = builder.build();

        TwitterFactory factory = new TwitterFactory(configurationBuilder);
        connection = factory.getInstance();
    }

    private long getDataFrom(Query query) throws TwitterException {

        QueryResult result = connection.search(query);

        long id = 0;

        for (Status status : result.getTweets()) {

            Message message = new Message();

            String bodyText = status.getText();

            if (status.getRetweetedStatus() != null) {
                bodyText = status.getRetweetedStatus().getText();
            }

            message.setBody(bodyText);
            message.addAttribute("date", String.valueOf(status.getCreatedAt()));
            message.addAttribute("userScreenName", status.getUser().getScreenName());
            message.addAttribute("idMessage", String.valueOf(status.getId()));

            Event.operation.publish("BotBase", message);

            if (id > status.getId() | id == 0) {
                id = status.getId();
            }
        }
        return id;
    }

    public void searchAll() {

        long id = 1;
        int cont = 0;
        while (id != 0 & cont < 20) {
            try {
                id = getDataFrom(query);
                query.setMaxId(id - 1);
                cont = cont + 1;
            } catch (TwitterException e) {
                id = 0;
                e.printStackTrace();
            }
        }
    }
}
