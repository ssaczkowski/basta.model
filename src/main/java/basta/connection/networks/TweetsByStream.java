package basta.connection.networks;

import avro.shaded.com.google.common.collect.ImmutableList;
import basta.structure.Message;
import basta.structure.channel.pubsubchannel.Event;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class TweetsByStream extends Thread {

    private static final String CONSUMER_KEY = "XHQ6DGnOInfr3xmlv6Tjkf86y";
    private static final String CONSUMER_SECRET = "E8thwH5N8ig8IaXhazTMWIbOOMSEJVGTZOF3loNVtr1evaSoTl";
    private static final String TOKEN = "1119972818404376576-oCV1fJ9QRmuR7ux3JxFfCITZtCMzUI";
    private static final String TOKEN_SECRET = "ZYsLreqLDUBmvzunR1qrHpM84KjuPDVBlwMFLsUwS2fkE";
    private Client client;
    private StatusesFilterEndpoint endpoint;
    private BlockingQueue<String> queue;
    private int cont = 0;

    @Override
    public void run() {
        super.run();
        try {
            getMessagesByStream();
            this.cont = 0;
            sleep(900000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startStream() {
        this.start();
    }

    public void stopStream() {
        this.disconnect();
    }

    private void connect() {
        queue = new LinkedBlockingQueue<>(10000);

        endpoint = new StatusesFilterEndpoint();

        Authentication auth = new OAuth1(CONSUMER_KEY, CONSUMER_SECRET, TOKEN, TOKEN_SECRET);

        client = new ClientBuilder()
                .hosts(Constants.STREAM_HOST)
                .endpoint(endpoint)
                .authentication(auth)
                .processor(new StringDelimitedProcessor(queue))
                .build();

        client.connect();
    }

    private void disconnect() {
        if (client != null) {
            client.stop();
        }
        client = null;
    }

    @SuppressWarnings("all")
    private void getMessagesByStream() {

        connect();

        this.getEndpoint().trackTerms(ImmutableList.of("@")).languages(ImmutableList.of("es"));

        while (isConnect() && this.cont < 300) {

            String msgJsonString;
            try {
                if (!isConnect()) {
                    System.out.println("Client connection closed unexpectedly. ");
                    break;
                }

                msgJsonString = getQueue().poll(5, TimeUnit.SECONDS);
                if (msgJsonString == null) {
                    System.out.println("Did not receive a message in 5 seconds.");
                } else {

                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject json;
                        json = parser.parse(msgJsonString).getAsJsonObject();

                        Message message = new Message();
                        message.setBody(json.get("text").getAsString());
                        message.addAttribute("user", json.getAsJsonObject("user").get("screen_name").getAsString());
                        message.addAttribute("date", json.get("created_at").getAsString());
                        message.addAttribute("sequenceName", "StreamChannel");

                        System.out.println(message.getBody());

                        Event.operation.publish("StreamingTweets", message);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                this.cont = this.cont + 1;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private StatusesFilterEndpoint getEndpoint() {
        return endpoint;
    }

    private BlockingQueue<String> getQueue() {
        return queue;
    }

    private boolean isConnect() {
        return client != null;
    }
}
