package basta.connection.networks;

import basta.connection.netconfiguration.Configuration;
import basta.structure.Message;
import basta.util.ConfigurationMnemonic;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.ArrayList;
import java.util.List;

public class TweetsByUser {

    private Twitter connection;
    private Query query;

    public TweetsByUser(long maxId, String name) {
        this.connectTwitter();
        this.setTimelineQuery(maxId, name);
    }

    private void setTimelineQuery(long maxId, String name) {
        query = new Query();
        query.setCount(100);
        query.setQuery(name);
        query.setMaxId(maxId - 1);
    }

    private void connectTwitter() {

        Configuration configuration = new Configuration();

        configuration.putParameter(ConfigurationMnemonic.CONSUMER_KEY, "XHQ6DGnOInfr3xmlv6Tjkf86y");
        configuration.putParameter(ConfigurationMnemonic.CONSUMER_SECRET, "E8thwH5N8ig8IaXhazTMWIbOOMSEJVGTZOF3loNVtr1evaSoTl");
        configuration.putParameter(ConfigurationMnemonic.TOKEN_KEY, "1119972818404376576-oCV1fJ9QRmuR7ux3JxFfCITZtCMzUI");
        configuration.putParameter(ConfigurationMnemonic.TOKEN_SECRET, "ZYsLreqLDUBmvzunR1qrHpM84KjuPDVBlwMFLsUwS2fkE");

        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.setOAuthConsumerKey(configuration.getParameter(ConfigurationMnemonic.CONSUMER_KEY));
        builder.setOAuthConsumerSecret(configuration.getParameter(ConfigurationMnemonic.CONSUMER_SECRET));
        builder.setOAuthAccessToken(configuration.getParameter(ConfigurationMnemonic.TOKEN_KEY));
        builder.setOAuthAccessTokenSecret(configuration.getParameter(ConfigurationMnemonic.TOKEN_SECRET));
        builder.setTweetModeExtended(true);
        twitter4j.conf.Configuration configurationBuilder = builder.build();

        TwitterFactory factory = new TwitterFactory(configurationBuilder);
        connection = factory.getInstance();
    }

    public List<Message> getDataFrom() throws TwitterException {

        QueryResult result = connection.search(this.query);
        ArrayList<Message> mensajes = new ArrayList<>();

        for (Status status : result.getTweets()) {

            Message message = new Message();

            String bodyText = status.getText();

            if (status.getRetweetedStatus() != null) {
                bodyText = status.getRetweetedStatus().getText();
            }

            message.setBody(bodyText);

            mensajes.add(message);
        }
        return mensajes;
    }
}