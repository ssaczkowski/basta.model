package basta.connection.database.dao;

import basta.connection.database.connection.MongoConnection;
import basta.model.Assignment;
import basta.model.Cycle;
import basta.model.Prediction;
import basta.structure.Message;
import com.mongodb.*;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.ArrayList;

public class MongoDAO {

    private static final MongoConnection conexion = MongoConnection.getConnection();

    public boolean existAgg(String aggressor) {
        try {
            DBCollection collection = conexion.getDatabase().getCollection("aggressor");
            BasicDBObject whereQuery = new BasicDBObject();
            whereQuery.append("userScreenName", aggressor);
            long found = collection.count(whereQuery);
            return found != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void insertMessage(String body) {
        try {
            DBCollection collection = conexion.getDatabase().getCollection("message");

            BasicDBObject document = new BasicDBObject();
            document.put("body", body);
            document.put("state", "free");

            BasicDBObject documentDetail = new BasicDBObject();

            documentDetail.put("chat_1", "");
            documentDetail.put("chat_2", "");
            documentDetail.put("chat_3", "");
            documentDetail.put("chat_4", "");
            documentDetail.put("chat_5", "");
            documentDetail.put("chat_6", "");
            documentDetail.put("chat_7", "");
            documentDetail.put("chat_8", "");
            documentDetail.put("chat_9", "");
            documentDetail.put("chat_10", "");

            document.put("assigned", documentDetail);

            collection.insert(document);
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    public boolean dataBaseLow() {
        try {
            DBCollection collection = conexion.getDatabase().getCollection("message");
            BasicDBObject whereQuery = new BasicDBObject();
            whereQuery.append("state", "free");
            long found = collection.count(whereQuery);
            return found < 1000;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public ArrayList<Message> getNoReFitMessages() {

        ArrayList<Message> messages = new ArrayList<>();

        try {
            DBCollection collection = conexion.getDatabase().getCollection("message_etiqueta");
            BasicDBObject whereQuery = new BasicDBObject();
            whereQuery.append("state", "noReFit");
            DBCursor objects = collection.find(whereQuery);

            for (DBObject d : objects) {
                Message m = new Message();
                m.setBody(String.valueOf(d.get("body")));
                m.addAttribute("_id", String.valueOf(d.get("_id")));
                m.addAttribute("label", String.valueOf(d.get("label")));
                messages.add(m);
            }

            return messages;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return messages;
    }

    public void updateStateMessage(List<Message> messages, String state) {

        try {
            DBCollection collection = conexion.getDatabase().getCollection("message_etiqueta");
            BasicDBObject whereQuery;
            BasicDBObject update;

            for (Message m : messages) {
                whereQuery = new BasicDBObject().append("_id", m.getHeaderMessage().getAttributes().get("_id"));
                update = new BasicDBObject().append("_id", m.getHeaderMessage().getAttributes().get("_id"))
                        .append("body", m.getHeaderMessage().getAttributes().get("body"))
                        .append("label", m.getHeaderMessage().getAttributes().get("label"))
                        .append("state", state);
                collection.update(whereQuery, update);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertPrediction(Prediction p) {
        try {
            DBCollection collection = conexion.getDatabase().getCollection("predictions");

            BasicDBObject document = new BasicDBObject();
            document.put("user", p.getUser());
            document.put("message", p.getMessage());
            document.put("prediction", p.getValue());

            collection.insert(document);
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Prediction> getPredictions() {

        ArrayList<Prediction> predictions = new ArrayList<>();

        try {
            DBCollection collection = conexion.getDatabase().getCollection("predictions");
            DBCursor col = collection.find();

            for (DBObject d : col) {
                String m = (String) d.get("message");
                String u = (String) d.get("user");
                String p = (String) d.get("prediction");

                Prediction pred = new Prediction(m, p, u);
                predictions.add(pred);
            }
            return predictions;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return predictions;
    }

    public void insertCicle(String user, ArrayList<String> msgs, String fase) {
        try {
            DBCollection aggCollection = conexion.getDatabase().getCollection("aggressor");
            DBCollection cycleCollection = conexion.getDatabase().getCollection("cycles");

            if (!existAgg(user)) {
                BasicDBObject aggDocument = new BasicDBObject();
                aggDocument.put("userScreenName", user);
                aggCollection.insert(aggDocument);
            }

            BasicDBObject cycleDocument = new BasicDBObject();
            cycleDocument.put("userScreenName", user);

            BasicDBObject documentDetail = new BasicDBObject();

            for (int i = 0; i < msgs.size(); i++) {
                documentDetail.put("mensaje_" + i, msgs.get(i));
            }
            cycleDocument.put("mensajes", documentDetail);
            cycleDocument.put("fase", fase);

            cycleCollection.insert(cycleDocument);
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Assignment> getAssignments() {

        ArrayList<Assignment> assignments = new ArrayList<>();

        try {
            DBCollection etiCollection = conexion.getDatabase().getCollection("etiqueta");
            DBCollection msgCollection = conexion.getDatabase().getCollection("message");
            DBCursor col = etiCollection.find();

            for (DBObject d : col) {
                String u = String.valueOf(d.get("chat_id"));
                String l = (String) d.get("etiqueta");

                BasicDBObject whereQuery = new BasicDBObject();
                whereQuery.append("_id", new ObjectId((String) d.get("id_mensaje")));

                String t = (String) msgCollection.find(whereQuery).next().get("body");

                Assignment assig = new Assignment(u, t, l);
                assignments.add(assig);
            }
            return assignments;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return assignments;
    }
/*
    public ArrayList<Cycle> getCycles() {

        ArrayList<Cycle> cycles = new ArrayList<>();

        try {
            DBCollection collection = conexion.getDatabase().getCollection("cycles");
            DBCursor col = collection.find();

            for (DBObject d : col) {
                String m = (String) d.get("message");
                String u = (String) d.get("user");
                String p = (String) d.get("prediction");

                Prediction pred = new Prediction(m, p, u);
                predictions.add(pred);
            }
            return predictions;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return predictions;
    }
*/
}