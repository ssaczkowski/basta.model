package basta.connection.netconfiguration;

import java.util.HashMap;
import java.util.Map;

public class Configuration {

    private Map<String,String> parameters;

    public Configuration()
    {
        this.parameters = new HashMap<>();
    }

    public void putParameter(String key, String value)
    {
        this.parameters.put(key,value);
    }

    public String getParameter(String key)
    {
        return this.parameters.get(key);
    }

}
