import basta.filters.HashtagSpamFilter;
import basta.filters.RepeatedSpamFilter;
import basta.structure.Message;
import org.junit.Assert;
import org.junit.Test;


public class RepeatedSpamFilterTest {


    @Test
    public final void discardHashtagMessage() {

        Message m1 = new Message();
        Message m2 = new Message();
        Message m3 = new Message();
        Message m4 = new Message();
        Message m5 = new Message();

        m1.setBody("RT: @user la vida es bella.");
        m2.setBody("RT: @user la vida no es bella.");
        m3.setBody("RT: @user la vida es bella.");
        m4.setBody("RT: @user la vida es bella.");
        m5.setBody("RT: @user la vida es bella y hermosa.");

        RepeatedSpamFilter spam = new RepeatedSpamFilter("Spam");

        spam.getIQueueIn().put(m1);
        spam.getIQueueIn().put(m2);
        spam.getIQueueIn().put(m3);
        spam.getIQueueIn().put(m4);
        spam.getIQueueIn().put(m5);

        spam.filter();

        Assert.assertEquals(3, spam.getIQueueOut().size());

    }
}
