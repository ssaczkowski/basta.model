import basta.filters.HashtagSpamFilter;
import basta.structure.Message;
import org.junit.Assert;
import org.junit.Test;


public class HashtagSpamFilterTest {


    @Test
    public final void discardHashtagMessage() {

        Message m = new Message();

        m.setBody("La vida es hermosa. #VIVILA#DISFRUTALA#HACEDEPORTE#FITNESS#SPORT#SOYSPAMER");

        HashtagSpamFilter spam = new HashtagSpamFilter("Spam");

        spam.getIQueueIn().put(m);

        spam.filter();

        Assert.assertEquals(0, spam.getIQueueOut().size());

    }

    @Test
    public final void noDiscardHashtagMessage() {

        Message m = new Message();

        m.setBody("La vida es hermosa. #VIVILA#DISFRUTALA#HACEDEPORTE#FITNESS");

        HashtagSpamFilter spam = new HashtagSpamFilter("Spam");

        spam.getIQueueIn().put(m);

        spam.filter();

        Assert.assertEquals(1, spam.getIQueueOut().size());

    }
}
