import basta.filters.RepetitiveCharFilter;
import basta.structure.Message;
import org.junit.Assert;
import org.junit.Test;


public class RepetitiveCharFilterTest {

    @Test
    public final void removeRepeatedChars() {

        Message m = new Message();
        m.setBody("Juannnnnnnnnnnnn             te quierooo muchooo sos el unico que cree en mi");
        RepetitiveCharFilter pre = new RepetitiveCharFilter("Pre");

        pre.getIQueueIn().put(m);

        pre.filter();

        Assert.assertEquals("Juan te quiero mucho sos el unico que cree en mi", m.getBody());

    }
}