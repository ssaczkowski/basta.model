import basta.filters.PreProcessFilter;
import basta.structure.Message;
import org.junit.Assert;
import org.junit.Test;


public class PreProcessFilterTest {

    @Test
    public final void removeCharacters() {

        Message m = new Message();
        m.setBody("@Juan -.,_:;][}{^^``~~´´+*¨¨´´¡¡¿¿?'=)(/&%$#!°¬\\|");

        PreProcessFilter pre = new PreProcessFilter("Pre");

        pre.getIQueueIn().put(m);

        pre.filter();

        Assert.assertEquals("", m.getBody());

    }

    @Test
    public final void removeURL() {

        Message m = new Message();
        m.setBody("#no lo puedo creer http://stackoverflow.com/questions/42775061/stanford-nlp-for-sentiment-analysis-of-tweets-stages");

        PreProcessFilter pre = new PreProcessFilter("Pre");

        pre.getIQueueIn().put(m);

        pre.filter();

        Assert.assertEquals("no lo puedo creer", m.getBody());

    }
}
