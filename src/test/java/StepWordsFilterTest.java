import basta.filters.StepWordsFilter;
import basta.structure.Message;
import org.junit.Assert;
import org.junit.Test;


public class StepWordsFilterTest {


    @Test
    public final void discardStepWords() {

        Message m = new Message();

        m.setBody("La casa del arbol es lo mas lindo que tiene el");

        StepWordsFilter step = new StepWordsFilter("Step");

        step.getIQueueIn().put(m);

        step.filter();

        Assert.assertEquals("casa arbol es mas lindo que tiene", step.getIQueueOut().getMessage().getBody());
    }
}
