import basta.filters.TranslateFilter;
import basta.structure.Message;
import org.junit.Assert;
import org.junit.Test;

public class TranslateFilterTest {

    @Test
    public void translate()
    {
        Message m = new Message();

        m.setBody("Amo traducir texto español al ingles.");

        TranslateFilter trans = new TranslateFilter("Translate");

        trans.getIQueueIn().put(m);

        trans.filter();

        Assert.assertEquals(1, trans.getIQueueOut().size());
    }
}
