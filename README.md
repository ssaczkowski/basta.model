# bastamodel

Sentiment Analysis BUS for Early Alerts

[![pipeline status](https://gitlab.com/ssaczkowski/basta.model/badges/master/pipeline.svg)](https://gitlab.com/ssaczkowski/basta.model/commits/master)

[![coverage report](https://gitlab.com/ssaczkowski/basta.model/badges/master/coverage.svg)](https://gitlab.com/ssaczkowski/basta.model/code-coverage-reports)


Some conventions to work on it:

- Follow existing coding conventions
- Add descriptive commits messages in English to every commit
- Write code and comments in English
- Write code using test